function banner(){
  message=$1
  echo ""
  echo "========================================"
  echo "  $message"
  echo "========================================"
  echo ""
}

function header(){
  message=$1
  echo ""
  echo "  $message"
  echo "----------------------------------------"
  echo ""
}
