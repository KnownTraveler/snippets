---
# BASE VARIABLES

# PROJECT DIRECTORY
# This is our ansible .git project repository
project: /home/<username>/<path_to_project>

# Workspace Directory
# This is our main workspace and is usually /home/username
workspace: /home/<username>

# Downloads Directory
# This is our main downloads and is usually /home/username/Downloads
downloads: /home/<username>/Downloads

# Name of the Role
role: base

# Filesytem Information
owner: <username>
group: <username>

# User Information
username: <username>
nickname: <alias_or_handle>
email: <email_address>

# SSH Key Name
keyname: id_rsa

# Command Line Utilities
# ----------------------------------
install_apt_utils: True
apt_utils:
- { name: "curl", target: "/usr/bin/curl", options: "-y" }
- { name: "gparted", target: "/usr/sbin/gparted", options: "-y" }
- { name: "jq", target: "/usr/bin/jq", options: "-y" }
- { name: "net-tools", target: "/usr/sbin/ifconfig", options: "-y" }
- { name: "nmap", target: "/usr/bin/nmap", options: "-y" }
- { name: "traceroute", target: "/usr/sbin/traceroute", options: "-y" }
- { name: "wget", target: "/usr/bin/wget", options: "-y" }
- { name: "whois", target: "/usr/bin/whois", options: "-y" }

install_snap_utils: True
snap_utils:
- { name: "atom", target: "/snap/bin/atom", options: "--classic" }
- { name: "brave", target: "/snap/bin/brave", options: "" }
- { name: "gimp", target: "/snap/bin/gimp", options: "" }
- { name: "remmina", target: "/snap/bin/remmina", options: "" }
- { name: "slack", target: "/snap/bin/slack", options: "--classic" }

hugo_version: 0.80.0
saml2aws_version: 2.27.1
terraform_version: 0.14.2

# Programming Language Configuration
# ----------------------------------
golang_version: 1.15.6

nodejs_version: 14.15.4
nodejs_distro: "linux-x64"

python_version: 3.7.7
install_pips: True

ruby_version: 3.0.0
install_gems: True

# SSH Configuration
# -----------------
ssh_root: "/home/<username>/.ssh"

ssh_dirs:
- { path: "/home/<username>/.ssh/<namespaceA>", owner: "<username>", group: "<username>" }
- { path: "/home/<username>/.ssh/<namespaceB>", owner: "<username>", group: "<username>" }

# SSK KEYS
#   ../files/ssh/<private>
#   ../files/ssh/<public>
ssh_keys:
- { private: "id_rsa_sample", public: "id_rsa_sample.pub", path: "/home/<username>/.ssh" }
- { private: "id_rsa_example", public: "id_rsa_example.pub", path: "/home/<username>/.ssh" }
- { private: "id_rsa_customA", public: "id_rsa_customA.pub", path: "/home/<username>/.ssh/namespaceA" }
- { private: "id_rsa_customB", public: "id_rsa_customB.pub", path: "/home/<username>/.ssh/namespaceB" }

# BitBucket Configuration
# -----------------------
bitbucket_root: "/home/<username>/BitBucket"

bitbucket_dirs:
- { path: "/home/<username>/BitBucket", owner: "<username>", group: "<username>" }

bitbucket_repos:
- { name: "bb-repo-a", source: "git@bitbucket.org:<namespace>/<repository_a>.git", target: "/home/<username>/BitBucket/bb-repo-a" }
- { name: "bb-repo-b", source: "git@bitbucket.org:<namespace>/<repository_b>.git", target: "/home/<username>/BitBucket/bb-repo-b" }
- { name: "bb-repo-c", source: "git@bitbucket.org:<namespace>/<repository_c>.git", target: "/home/<username>/BitBucket/bb-repo-c" }

# GitHub Configuration
# -----------------------
github_root: "/home/<username>/GitHub"

github_dirs:
- { path: "/home/<username>/GitHub", owner: "<username>", group: "<username>" }

github_repos:
- { name: "gh-repo-a", source: "git@github.com:<namespace>/<repository_a>.git", target: "/home/<username>/GitHub/gh-repo-a" }
- { name: "gh-repo-b", source: "git@github.com:<namespace>/<repository_b>.git", target: "/home/<username>/GitHub/gh-repo-b" }
- { name: "gh-repo-c", source: "git@github.com:<namespace>/<repository_c>.git", target: "/home/<username>/GitHub/gh-repo-c" }

# GitLab Configuration
# -----------------------
gitlab_root: "/home/<username>/GitLab"

gitlab_dirs:
- { path: "/home/<username>/GitLab", owner: "<username>", group: "<username>" }

gitlab_repos:
- { name: "gl-repo-a", source: "git@gitlab.com:<namespace>/<repository_a>.git", target: "/home/<username>/GitLab/gl-repo-a" }
- { name: "gl-repo-b", source: "git@gitlab.com:<namespace>/<repository_b>.git", target: "/home/<username>/GitLab/gl-repo-b" }
- { name: "gl-repo-c", source: "git@gitlab.com:<namespace>/<repository_c>.git", target: "/home/<username>/GitLab/gl-repo-c" }
