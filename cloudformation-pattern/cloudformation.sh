#!/usr/bin/env bash

## ARGUMENTS
## ====================================
ACTION=$1
STACK=$2
WORKSPACE=$3
if [ -z "$WORKSPACE" ]
then
      echo "Workspace argument not specified, using 'default'"
      WORKSPACE="default"
fi

## FUNCTIONS
## ====================================
function help () {
    echo "Description:"
    echo "    CloudFormation Script is a simple programmatic workflow"
    echo ""
    echo "Usage: "
    echo "    ./cloudformation.sh <ACTION> <STACK> <WORKSPACE>"
    echo ""
    echo "Example:"
    echo "    ./cloudformation.sh list-stacks"
    echo "    ./cloudformation.sh list-stack s3-bucket"
    echo "    ./cloudformation.sh list-parameters s3-bucket dev"
    echo "    ./cloudformation.sh deploy-stack s3-bucket dev"
    echo "    ./cloudformation.sh describe-stack s3-bucket"
    echo "    ./cloudformation.sh describe-stacks"
    echo "    ./cloudformation.sh destroy-stack s3-bucket dev"
    echo "    ./cloudformation.sh stage-stack s3-bucket dev"
    echo "    ./cloudformation.sh validate-template s3-bucket"
    echo ""
}

function describeStack () {
    echo ""
    echo "CloudFormation Script is Describing Stack '$STACK' in $WORKSPACE"
    COMMAND="aws cloudformation describe-stacks --stack-name $STACK "
    echo $COMMAND
    eval $COMMAND
}

function describeStacks () {
    echo ""
    echo "CloudFormation Script is Describing Stacks"
    COMMAND="aws cloudformation describe-stacks"
    echo $COMMAND
    eval $COMMAND
}

function stageStack () {
    echo ""
    echo "CloudFormation Script is getting Parameters for $STACK in $WORKSPACE "
    PARAMS=$(jq -r '.[] | [.ParameterKey, .ParameterValue] | "\(.[0])=\"\(.[1])\""' ./stacks/$STACK/params/$WORKSPACE.json)
    echo "-------------------------------------"
    echo "./stacks/$STACK/params/$WORKSPACE.json"
    echo "-------------------------------------"
    echo "$PARAMS"

    echo ""
    echo "CloudFormation Script is Staging Stack '$STACK' in $WORKSPACE "
    COMMAND="aws cloudformation deploy --stack-name $STACK --template-file ./stacks/$STACK/template.yaml --no-execute-changeset --parameter-overrides $PARAMS"
    echo $COMMAND
    echo ""
    eval $COMMAND
}

function deployStack () {
    echo ""
    echo "CloudFormation Script is getting Parameters for $STACK in $WORKSPACE "
    PARAMS=$(jq -r '.[] | [.ParameterKey, .ParameterValue] | "\(.[0])=\"\(.[1])\""' ./stacks/$STACK/params/$WORKSPACE.json)
    echo "-------------------------------------"
    echo "./stacks/$STACK/params/$WORKSPACE.json"
    echo "-------------------------------------"
    echo "$PARAMS"

    echo ""
    echo "CloudFormation Script is Deploying Stack '$STACK' in $WORKSPACE "
    COMMAND="aws cloudformation deploy --stack-name $STACK --template-file ./stacks/$STACK/template.yaml --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM --parameter-overrides $PARAMS"
    echo $COMMAND
    echo ""
    eval $COMMAND
}

function destroyStack () {
    echo ""
    echo "CloudFormation Script is Destroying Stack '$STACK' in $WORKSPACE"
    COMMAND="aws cloudformation delete-stack --stack-name $STACK "
    echo $COMMAND
    eval $COMMAND
}

function describeStacks () {
    echo ""
    echo "CloudFormation Script is Describing Stacks"
    COMMAND="aws cloudformation describe-stacks"
    echo $COMMAND
    eval $COMMAND
}

function describeStack () {
    echo ""
    echo "CloudFormation Script is Describing Stack '$STACK'"
    COMMAND="aws cloudformation describe-stack --stack-name $STACK"
    echo $COMMAND
    eval $COMMAND
}

function listParameters () {
    echo ""
    echo "CloudFormation Script is Listing Parameters for Stack '$STACK' in $WORKSPACE"
    PARAMS=$(jq -r '.[] | [.ParameterKey, .ParameterValue] | "\(.[0])=\"\(.[1])\""' ./stacks/$STACK/params/$WORKSPACE.json)
    echo "-------------------------------------"
    echo "./stacks/$STACK/params/$WORKSPACE.json"
    echo "-------------------------------------"
    echo $PARAMS
    echo ""
}

function validateTemplate () {
    echo ""
    echo "CloudFormation Script is Validating Template for Stack '$STACK'"
    COMMAND="aws cloudformation validate-template --template-body file://./stacks/$STACK/template.yaml"
    echo $COMMAND
    eval $COMMAND
}

case "$ACTION" in
        stage-stack)
            stageStack
            ;;
        deploy-stack)
            deployStack
            ;;
        describe-stack)
            describeStack
            ;;
        describe-stacks)
            describeStacks
            ;;    
        destroy-stack)
            destroyStack
            ;;
        list-stack)
            describeStack
            ;;
        list-stacks)
            describeStacks
            ;;
        list-parameters)
            listParameters
            ;;
        validate-template)
            validateTemplate
            ;;        
        *)
            help
            ;;
esac
