# AWS CloudFormation <Name_Of_Template> Template

## Table of contents

* [Summary](#summary)
* [Usage](#usage)
* [Components](#components)
* [Structure](#structure)
* [Issues](#issues)
* [Contributions](#contributions)
* [Questions](#questions)

## Summary

Author(s): Author Name (@Alias)
Email(s): Email Address

Use this section to provide a detailed overview of your template.

* What use case(s) or problem(s) does your solution pattern solve?
* What resources does your template provision?

## Usage

Examples of how to use this template:

## Components

Use this section to provide a detailed review of the core components of your template.

* Describe the 'resources' that are deployed using this template
* Describe the dependencies between resources
* Describe the default configuration of the resources
* Describe any special policy considerations (UpdatePolicy, DeletePolicy, etc)

## Structure

CloudFormation templates describe your AWS resources and their properties.  

Use this section to describe the structure of your Service Template:

    /stack                  # Stack Template Root.
            
        /parameters         # Parameters Directory for the CloudFormation Template
            default.json    # Default Parameters to be used when Environment Specific Parameters File Does Not Exist
                            # Environment Specific Parameter Files can be implemented via dev.json, tst.json, prd.json

        /policy             # Policy Directory for the CloudFormation Stack Policy
            default.json    # Default Stack Policy to be used when Environment Specific Policy File Does Not Exist
                            # Environment Specific Policy Files can be implemented via dev.json, tst.json, prd.json
            
        service.yaml        # Service Template Configuration File
        template.yaml       # CloudFormation Template for the Service
        README.md           # README File describing the service template

## Issues

Describe how users should report any issues that are identified while using this Service Template

Example:

If you identify an issue using this template please contact support@example.com

Detailed information is very helpful to understand and prioritize issues.

    How to reproduce the issue, step-by-step.
    The expected behavior (or what is wrong).
    The error message(s)
    The operating system
    Screenshots if applicable

## Contributions

Pull Requests (PRs) are always welcome.  Please create a new branch and submit a pull-request for review.

## Questions

Still have questions?  Please contact support@example.com