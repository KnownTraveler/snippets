# Snippets

## TLDR

This is a collection of code snippets for my personal blog hosted at https://knowntraveler.io

## CONTENTS

* [ansible-bootstrap](./ansible-bootstrap/README.md) 
* [ansible-docker](./ansible-docker/README.md) 
* [ansible-venv](./ansible-venv/README.md)
* [cloudformation-pattern](./cloudformation-pattern/README.md)
* [golang-cli](./golang-cli/README.md)
* [terraform-pattern](./terraform-pattern/README.md)

## LICENSE

This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. 

If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
