#!/usr/bin/bash

# SOURCE LIBRARY FUNCTIONS
. ./scripts/_lib.sh

banner "Ansible Script"

# INSTALL VIRTUALENV MODULE
if [ ! -f "$HOME/.pyenv/shims/virtualenv" ]
then
    header "Python Module virtualenv (venv) missing"
    header "Installing virtualenv using pip"
    pip install virtualenv
else
    header "Python Module virtualenv (venv) installed"
fi

# ACTIVATE VIRTUAL ENVIRONMENT
if [ ! -f "./venv/bin/activate" ]
then
    header "Creating Virtual Environment"
    python -m virtualenv venv

    header "Activating Virtual Environment"
    source ./venv/bin/activate
else 
    header "Activating Virtual Environment"
    source ./venv/bin/activate
fi

# INSTALL REQUIREMENTS
header "Installing Requirements"
pip install -r requirements.txt

# RUN ANSIBLE
header "Running Ansible"
ansible-playbook -i "localhost," -c local playbook_sample.yaml -e "ansible_python_interpreter=$HOME/GitLab/knowntraveler/snippets/ansible-venv/venv/bin/python" --ask-become-pass

# DEACTIVATE VIRTUAL ENVIRONMENT
header "Deactivating Virtual Environment"
deactivate
