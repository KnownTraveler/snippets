#!/usr/bin/env bash

# CONSTANTS
PYTHON_VERSION=3.7.7

# SOURCE LIBRARY FUNCTIONS
. ./scripts/_lib.sh

banner "Init Script"

# Install Package curl
if [ ! -f "/usr/bin/curl" ]
then
    header "Package 'curl' not installed at /usr/bin/curl"
    header "Installing curl at /usr/bin/curl"
    sudo apt install curl -y
else
    header "Package 'curl' already installed at /usr/bin/curl"
fi

# Install Package git
if [ ! -f "/usr/bin/git" ]
then
    header "Package 'git' not installed at /usr/bin/git"
    header "Installing git at /usr/bin/git"
    sudo apt install git -y
else
    header "Package 'git' already installed at /usr/bin/git"
fi

# Install pyenv
if [ ! -d "$HOME/.pyenv" ]
then
    header "PYENV is not installed at $HOME/.pyenv"
    header "Installing 'pyenv' at $HOME/.pyenv"
    mkdir "$HOME/.pyenv"
    git clone https://github.com/pyenv/pyenv.git "$HOME/.pyenv"
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc
else
    header "PYENV is already installed at $HOME/.pyenv"
fi

# Install python, virtualenv ansible
if [ ! -d "$HOME/.pyenv/versions/$PYTHON_VERSION" ]
then
    header "Python $PYTHON_VERSION is not installed at $HOME/.pyenv/versions/$PYTHON_VERSION"
    header "Updating apt cache"
    sudo apt-get update
    sudo apt-get install --no-install-recommends make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev -y
    
    header "Installing python $PYTHON_VERSION via pyenv"
    $HOME/.pyenv/bin/pyenv install $PYTHON_VERSION
    $HOME/.pyenv/bin/pyenv global $PYTHON_VERSION
    
    header "Upgrading PIP"
    $HOME/.pyenv/versions/$PYTHON_VERSION/bin/pip install --upgrade pip
    
    header "Installing 'virtualenv' (venv) via pip"
    $HOME/.pyenv/versions/$PYTHON_VERSION/bin/pip install virtualenv
    
    header "Installing ansible' via pip"
    $HOME/.pyenv/versions/$PYTHON_VERSION/bin/pip install ansible

else
    header "Python $PYTHON_VERSION is already installed at $HOME/.pyenv/versions/$PYTHON_VERSION"
fi
