#!/usr/bin/env bash

## CONSTANTS
## ====================================
TERRAFORM_VERSION=0.12.0
PROJECT_PATH=$(dirname $(readlink -f $0))
GLOBAL_PATH=$PROJECT_PATH/global
OS_SHORTNAME=$(uname | awk '{print tolower($0)}')


## ARGUMENTS
## ====================================
WORKSPACE=$1
STACK=$2
ACTION=$3
ACTION_ARGS=$(echo "$@" | awk '{first = $1; second = $2; third = $3; $1 = ""; $2 = ""; $3 = ""; print $0 }')

set -e

## TERRAFORM VERSION (LOCAL)
## ====================================
CURRENT_VERSION=$($PROJECT_PATH/bin/terraform --version 2>/dev/null | head -1 | awk '{print $2}' | sed 's/^v//')
if [ ! "$CURRENT_VERSION" == "$TERRAFORM_VERSION" ]; then
FILE=$(mktemp)
echo "This project is currently using terraform v$TERRAFORM_VERSION. You're at v$CURRENT_VERSION. Installing terraform@${TERRAFORM_VERSION}..."
if [ "$(uname)" == "Darwin" ]; then
    curl -L https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_darwin_amd64.zip -o $FILE
    unzip -o $FILE -d /tmp terraform
    mv /tmp/terraform $PROJECT_PATH/bin/terraform
    rm -rf $FILE
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    curl -L https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o $FILE
    unzip -o $FILE -d /tmp terraform
    mv /tmp/terraform $PROJECT_PATH/bin/terraform
    rm -rf $FILE
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    echo "Appears that you are running windows. Have you tried turning it off an on again?"
fi
fi


## TERRAFORM WRAPPER
## ====================================

echo ""
echo "=============================================================================="
echo " Executing terraform $ACTION against the $STACK stack in $WORKSPACE. "
echo "=============================================================================="
echo ""

## EXPORT VARIABLES
export STACK
export TARGET_DIR=$PROJECT_PATH/workspaces/$WORKSPACE/$STACK

## VERIFY TARGET DIRECTORY EXISTS (e.g. STACK)
if [ ! -d "$TARGET_DIR" ]; then
echo "Error: Stack \"$STACK\" does not exist in workspace \"$WORKSPACE\"."
exit 1
fi

## CHANGE TO TARGET DIRECTORY
cd $TARGET_DIR

## FORMAT TERRAFORM COMMAND
export CMD="$PROJECT_PATH/bin/terraform $ACTION $ACTION_ARGS"

## VERIFY TERRAFORM STACK IS INITIALIZED
if [ ! -d "$PROJECT_PATH/workspaces/$WORKSPACE/$STACK/.terraform" ]; then
echo "initalizing stack"
$PROJECT_PATH/bin/terraform init
fi

## GET UPDATES FOR TERRAFORM MODULES 
$PROJECT_PATH/bin/terraform get -update

set +e

## SELECT TERRAFORM WORKSPACE, ELSE CREATE WORKSPACE
$PROJECT_PATH/bin/terraform workspace select $WORKSPACE >/dev/null 2>&1
if [ "$?" -eq "1" ]; then
echo "creating new workspace: $WORKSPACE for stack $STACK"
$PROJECT_PATH/bin/terraform workspace new $WORKSPACE
fi

set -e

## TERRAFORM COMMAND
$CMD