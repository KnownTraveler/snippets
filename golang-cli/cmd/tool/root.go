// Copyright © 2020 Brian Hooper <knowntraveler.io>
// Author: Brian Hooper (@KnownTraveler)
// Project: gitlab.com/knowntraveler/snippets/golang-cli

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/knowntraveler/snippets/golang-cli/log"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tools",
	Short: "TOOL is a Command Line Utility that equips you to automate something awesome in your workspace",
	Long: `
TOOL COMMAND LINE UTILITY

# AUTHOR
Brian Hooper (@KnownTraveler) https://knowntraveler.io

# LICENSE
The Supply Source Code is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/. 

_______________________________________________________________________________

SUMMARY

TOOL is designed to be a lightweight tool that equips practitioners...

ADD MORE...

TOOL is a command line utility written in Go using Cobra/Viper, 
which are libraries for Go that empower command line applications.
- Cobra https://github.com/spf13/cobra
- Viper https://github.com/spf13/viper

_______________________________________________________________________________

TERMINOLOGY

# TOOL (verb)
- equip or be equipped
- impress a design on

# SUPPLY (noun)
- a device or implement used to carry out a particular function
- an instrument or simple piece of equipment to do a particular kind of work

ADD MORE...

_______________________________________________________________________________

FEATURES

[TOOL VERSION]
Provides the version of the TOOL Command Line Utility

ADD MORE...

`,

	PersistentPreRun: func(cmd *cobra.Command, args []string) {

		// Configure Verbose Logging Option from CLI
		if options.verbose {
			log.EnableVerbose()
		}

		// Configure Logging options from Environment
		logLevel := os.Getenv("TOOL_DEBUG")

		if logLevel == "verbose" {
			log.EnableVerbose()
		}

		if logLevel == "debug" {
			log.EnableVerbose()
			log.EnableDebug()
		}

		if logLevel == "trace" {
			log.EnableVerbose()
			log.EnableDebug()
			log.EnableTrace()
		}

	},
}

type rootFlags struct {
	verbose              bool
	nonInteractive       bool
	overrideConfirmation bool
}

var options rootFlags

func init() {
	// Commands
	rootCmd.AddCommand(CmdVersion)

	// PersistentFlags
	rootCmd.PersistentFlags().BoolVarP(&options.verbose, "verbose", "v", false, "Enable verbose output")
	rootCmd.PersistentFlags().BoolVar(&options.nonInteractive, "non-interactive", false, "Disable interactive prompts")
	rootCmd.PersistentFlags().BoolVarP(&options.overrideConfirmation, "yes", "y", false, "Override confirmations")

}
