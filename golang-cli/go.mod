module gitlab.com/knowntraveler/snippets/golang-cli

go 1.15

require (
	github.com/golangci/golangci-lint v1.36.0 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/onsi/ginkgo v1.14.2
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/lint v0.0.0-20201208152925-83fdc39ff7b5 // indirect
	golang.org/x/tools v0.1.0 // indirect
)
