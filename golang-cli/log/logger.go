// Copyright © 2020 Brian Hooper <knowntraveler.io>
// Author: Brian Hooper (@KnownTraveler)
// Project: gitlab.com/knowntraveler/snippets/golang-cli

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package log (gitlab.com/knowntraveler/snippets/golang-cli/log) provides a uniform
// Logging API that has been customized for the TOOL CLI to print
// messages to the user via stdout
package log

import (
	"fmt"
	"log"
	"os"

	auroraPackage "github.com/logrusorgru/aurora"
	colorable "github.com/onsi/ginkgo/reporters/stenographer/support/go-colorable"
	isatty "github.com/onsi/ginkgo/reporters/stenographer/support/go-isatty"
)

var aurora auroraPackage.Aurora

// Flag to Enable Verbose Logging
var verboseEnabled bool

// Flag to Enable Debug Logging
var debugEnabled bool

// Flog to Enable Trace Logging
var traceEnabled bool

func init() {
	aurora = auroraPackage.NewAurora(isatty.IsTerminal(os.Stdout.Fd()))
	log.SetOutput(colorable.NewColorableStdout())
	log.SetFlags(0)
}

// Print logs a message at level Info
func Print(message string) {
	str := fmt.Sprintf(aurora.BrightCyan("%v").String(), message)
	log.Printf("%s", str)
}

// Printf logs a formatted message at level Info
func Printf(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightCyan("%v").String(), message)
	log.Printf("%s", str)
}

// VPrint logs a message at level Info when verboseEnabled is true
func VPrint(message string) {
	str := fmt.Sprintf(aurora.BrightCyan("INFO: %v").String(), message)
	if verboseEnabled {
		log.Printf("%s", str)
	}
}

// VPrintf logs a message at level Info when verboseEnabled is true
func VPrintf(format string, args ...interface{}) {
	if verboseEnabled {
		message := fmt.Sprintf(format, args...)
		str := fmt.Sprintf(aurora.BrightCyan("INFO: %v").String(), message)
		log.Printf("%s", str)
	}
}

// Success logs a message at level Info
func Success(message string) {
	str := fmt.Sprintf(aurora.BrightGreen("SUCCESS: %v").String(), message)
	log.Printf("%s", str)
}

// Successf logs a formatted message at level Info
func Successf(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightGreen("SUCCESS: %v").String(), message)
	log.Printf("%s", str)
}

// Warning logs a message at level Warn
func Warning(message string) {
	str := fmt.Sprintf(aurora.BrightYellow("WARNING: %v").String(), message)
	log.Printf("%s", str)
}

// Warningf logs a formatted message at level Warn
func Warningf(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightYellow("WARNING: %v").String(), message)
	log.Printf("%s", str)
}

// Failure logs a message at level Error
func Failure(message string) {
	str := fmt.Sprintf(aurora.BrightRed("FAILURE: %v").String(), message)
	log.Printf("%s", str)
}

// Failuref logs a formatted message at level Error
func Failuref(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightRed("FAILURE: %v").String(), message)
	log.Printf("%s", str)
}

// Error logs a message at level Error
func Error(message string) {
	str := fmt.Sprintf(aurora.BrightRed("ERROR: %v").String(), message)
	log.Printf("%s", str)
}

// Errorf logs a message at level Error
func Errorf(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightRed("ERROR: %v").String(), message)
	log.Printf("%s", str)
}

// Panic logs a message at level Panic
func Panic(message string) {
	str := fmt.Sprintf(aurora.BrightRed("PANIC: %v").String(), message)
	log.Panicf("%s", str)
}

// Panicf logs a formatted message at level Panic
func Panicf(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightRed("PANIC: %v").String(), message)
	log.Panicf("%s", str)
}

// Fatal logs a message at level Fatal
func Fatal(message string) {
	str := fmt.Sprintf(aurora.BrightRed("FATAL: %v").String(), message)
	log.Fatalf("%s", str)
}

// Fatalf logs a formatted message at level Fatal
func Fatalf(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	str := fmt.Sprintf(aurora.BrightRed("FATAL: %v").String(), message)
	log.Fatalf("%s", str)
}

// Debug logs a message at level Debug
func Debug(message string) {
	if debugEnabled {
		log.Printf("DEBUG: %s", message)
	}
}

// Debugf logs a formatted message at level Debug
func Debugf(format string, args ...interface{}) {
	if debugEnabled {
		message := fmt.Sprintf(format, args...)
		log.Printf("DEBUG: %s", message)
	}
}

// Trace logs a message at level Trace
func Trace(message string) {
	if traceEnabled {
		log.Printf("TRACE: %s", message)
	}
}

// Tracef logs a formatted message at level Trace
func Tracef(format string, args ...interface{}) {
	if traceEnabled {
		message := fmt.Sprintf(format, args...)
		log.Printf("TRACE: %s", message)
	}
}

// EnableVerbose turns on verbose logging
func EnableVerbose() {
	verboseEnabled = true
}

// EnableDebug turns on enable logging
func EnableDebug() {
	debugEnabled = true
}

// EnableTrace turns on trace logging
func EnableTrace() {
	traceEnabled = true
}
