
# GOLANG-CLI SNIPPET

## TLDR

This snippet covers setting up a new Command Line Utility (CLI) Project using [Golang](https://golang.org/) and [Cobra](https://github.com/spf13/cobra) which is a library for creating powerful modern CLI applications as well as a program to generate applications and command files. [Cobra](https://github.com/spf13/cobra) is used in many Go projects for example:

* Docker
* Golangci-lint
* Helm
* Hugo
* Kubernetes
* OpenShift
* ... just to name a few

## RESOURCES

* [Golang](https://golang.org/)
* [Golang Cobra Library](https://github.com/spf13/cobra)
* [Golang Viper Library](https://github.com/spf13/viper)
* [Golangci-Lint](https://github.com/golangci/golangci-lint)

## PREREQUISITES

### Install Golang

* [Download golang ](https://golang.org/dl/)
* [Install golang](https://golang.org/doc/install)

Verify Golang Installation

    $ which go
    /usr/local/go/bin/go

Verify Golang Version

    $ go version
    go version go1.15.6 linux/amd64


### Set Environment Variables

Set golang environment variables in ~/.bashrc

    # GOLANG
    # ----------------------------------------

    # Setting GOPATH
    export GOPATH=$HOME/.go

    # Setting GOROOT
    export GOROOT=/usr/local/go

    # Setting PATH for GOLANG BINARY
    export PATH="$GOPATH/bin:$GOROOT/bin:$PATH"


Verify golang environment variables

    $ echo $GOROOT
    /usr/local/go

    $ echo $GOPATH
    /home/<user>/.go

## CREATE PROJECT

### CREATE PROJECT FOLDER

    $ mkdir golang-cli

### CREATE GOLANG MODULE

    $ go mod init gitlab.com/knowntraveler/snippets/golang-cli

### INSTALL GOLANG LINTER

    $ go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.36.0
    $ which golangci-lint
    /home/<user>/.go/bin/golangci-lint

### STRUCTURE

    ./golang-cli
    |__ /bin
    |   |__ /darwin
    |   |   |__ tool-darwin-amd64       <-- Darwin Binary (make compile)
    |   |__ /linux
    |   |   |__ tool-linux-amd64        <-- Linux Binary (make compile)        
    |   |__ /windows
    |   |   |__ tool-windows-amd64.exe  <-- Windows Binary (make compile)
    |   |
    |   |__ tool                        <-- Symlink to Tool Binary (make)
    |   |__ tool-${GOOS}-${GOARCH}      <-- Tool Binary (make)
    |
    |__ /cmd
    |   |__ /tool                       <-- Command Line Utility
    |       |__ main.go
    |       |__ root.go
    |       |__ version.go
    |
    |__ /log                            <-- Log Package, Custom for CLI
    |__ /scripts
    |   |__ /build                      <-- Build Scripts
    |   |__ /deploy                     <-- Deploy Scripts        
    |__ /version                        <-- Version Package
    |__ .gitignore
    |__ .golangci.yaml                  <-- Linter Configuration
    |__ go.mod
    |__ go.sum
    |__ LICENSE
    |__ Makefile
    |__ README.md

## BUILDING THE PROJECT

This project uses `make` to define and execute our build tasks.

### MAKE

Using the default `make` runs the following tasks:

* clean     - clean `/bin` directory
* format    - format .go files using `go fmt` 
* lint      - lint .go files using `golangci-lint`
* test      - test _test.go files using `go test`
* build     - build binary target `/bin/tool-${GOOS}-${GOARCH}` via `/scripts/build/binary.sh`
* link      - link binary target `/bin/tool` and `$GOPATH/bin/tool`

### MAKE COMPILE

Using `make compile` will run the following scripts:

* For Linux:
    * Executes `/scripts/build/linux.sh`
    * Outputs `/bin/linux/tool-linux-amd64`
* For MacOS:
    * Executes `/scripts/build/darwin.sh`
    * Outputs `/bin/linux/tool-darwin-amd64`
* For Windows:
    * Executes `/scripts/build/windows.sh`
    * Outputs `/bin/linux/tool-windows-amd64.exe`

### MAKE <TASK>

Using `make <task>` will run individual tasks:

    # Formatting
    $ make format

    # Linting
    $ make lint

    # Testing
    $ make test
