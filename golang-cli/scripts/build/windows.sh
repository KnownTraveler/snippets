#!/usr/bin/env bash
#
# Build a windows binary from linux
#
set -eu

source ./scripts/build/.variables

export GOOS=windows
export GOARCH=amd64
export LDFLAGS="-X 'version.Version=v1.0.0'"

# Override TARGET
TARGET="bin/windows/tool-$GOOS-$GOARCH.exe"

echo "Building $TARGET"
go build -o "${TARGET}" --ldflags "${LDFLAGS}" "${SOURCE}"
