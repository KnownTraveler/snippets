#!/usr/bin/env bash
#
# Build a static binary for the local host OS/ARCH
#
set -eu

source ./scripts/build/.variables

go build -o "${TARGET}" --ldflags "${LDFLAGS}" "${SOURCE}"

ln -sf "$(basename "${TARGET}")" bin/tool