#!/usr/bin/env bash
#
# Build a darwin (MacOS) binary from linux
#
set -eu

source ./scripts/build/.variables

export GOOS=darwin
export GOARCH=amd64
export LDFLAGS="-X 'version.Version=v1.0.0'"

# Override TARGET
TARGET="bin/darwin/tool-$GOOS-$GOARCH"

echo "Building $TARGET"
go build -o "${TARGET}" --ldflags "${LDFLAGS}" "${SOURCE}"
